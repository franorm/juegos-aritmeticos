package tpprograII;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JTextField;

public class PantallaInicio {

	private JFrame frame;
	private JLabel lblNewLabel = new JLabel("New label");
	private JLabel lblNewLabel_1 = new JLabel("El bid\u00F3n de Bilardo presenta...");
	private JButton botonJugar = new JButton("Jugar");	
	private JButton botonTutorial = new JButton("Reglas");
	private JLabel lblMatriz = new JLabel("Matriz");
	private JRadioButton RBDificil = new JRadioButton("Dif\u00EDcil");
	private JRadioButton RBFacil = new JRadioButton("F\u00E1cil");
	private JPanel panel = new JPanel();
	private final JButton botonComenzarPartida = new JButton("\u00A1Jugar!");
	private ButtonGroup grupo;
	private Integer dimensionesMatriz;
	private JSpinner spinner = new JSpinner();
	private JTextField inputNombre;
	private final JButton botonScoreboard = new JButton("Scoreboard");
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaInicio window = new PantallaInicio();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public JFrame getFrame() {
		return frame;
	}



	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Create the application.
	 */
	public PantallaInicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(frame);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		Juego juego=new Juego();
		
		panel.setBounds(51, 56, 141, 194);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		

		RBFacil.setSelected(true);
		RBFacil.setBounds(15, 61, 111, 23);
		panel.add(RBFacil);
		

		RBDificil.setBounds(15, 87, 111, 23);
		panel.add(RBDificil);
		
		spinner.setModel(new SpinnerNumberModel(4, 4, 9, 1));
		spinner.setBounds(92, 129, 30, 20);
		panel.add(spinner);
		
		lblMatriz.setBounds(15, 132, 48, 14);
		panel.add(lblMatriz);
		botonComenzarPartida.setBounds(25, 155, 91, 23);
		
		panel.add(botonComenzarPartida);
		

		botonTutorial.setBounds(267, 137, 109, 23);
		frame.getContentPane().add(botonTutorial);
		

		botonJugar.setBounds(267, 103, 109, 23);
		frame.getContentPane().add(botonJugar);
		botonScoreboard.setBounds(267, 171, 109, 23);
		
		frame.getContentPane().add(botonScoreboard);
		

		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		lblNewLabel_1.setIcon(null);
		lblNewLabel_1.setBounds(10, -16, 305, 61);
		frame.getContentPane().add(lblNewLabel_1);
		

		lblNewLabel.setIcon(new ImageIcon(PantallaInicio.class.getResource("/images/fondo.jpg")));
		lblNewLabel.setBounds(0, 0, 436, 264);
		frame.getContentPane().add(lblNewLabel);
		
		panel.setVisible(false);
		
		botonJugar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				panel.setVisible(true);
			}
			
		});

		
		grupo = new ButtonGroup();
		grupo.add(RBDificil);
		grupo.add(RBFacil);
		
		inputNombre = new JTextField();
		inputNombre.setBounds(15, 35, 111, 20);
		panel.add(inputNombre);
		inputNombre.setColumns(10);
		
		JLabel lblNombre = new JLabel("Ingrese nombre");
		lblNombre.setBounds(16, 12, 110, 14);
		panel.add(lblNombre);
		
		this.dimensionesMatriz = (Integer)this.spinner.getModel().getValue();
		
		this.spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				
				dimensionesMatriz = (Integer)spinner.getModel().getValue();
				
			}
			
		});
		
		this.botonComenzarPartida.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				pantallaJuego partida = new pantallaJuego(dimensionesMatriz, RBFacil.getModel().isSelected());
				partida.getFrame().setVisible(true);
				juego.setJugador(inputNombre.getText());
				RBDificil.setActionCommand("Dificil");
				RBFacil.setActionCommand("Facil");
				juego.setDificultad(grupo.getSelection().getActionCommand());
				frame.setVisible(false);
			}
		});
		
		this.botonScoreboard.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pantallaPuntaje puntaje=new pantallaPuntaje();
				puntaje.getFrame().setVisible(true);
				frame.setVisible(false);
				
			}
		});
		
		
		this.botonTutorial.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PantallaReglas reglas=new PantallaReglas();
				reglas.getFrame().setVisible(true);
				frame.setVisible(false);
				
			}
		});
		

	}
}
