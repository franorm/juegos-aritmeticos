package tpprograII;
import java.util.*;
import java.io.*;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.time.temporal.ChronoUnit;

import javax.swing.JTextField;

public class Juego {
	private Grilla grilla, buffer;
	private Integer dimensiones;
	private boolean ganar;
	public boolean finalizado;
	public static String jugador;
	public static String dificultad;
	Instant inicio, fin;
	static long duracion;
	
	public Juego(int dimensiones) {
		if(dimensiones < 4 || dimensiones > 9) throw new IllegalArgumentException("La grilla no puede ser de "+dimensiones+" x "+" dimensiones.");
		this.setGrilla(dimensiones);
		this.setBuffer(dimensiones);
		this.ganar = false;
		this.dimensiones = dimensiones;
	}
	
	public Juego() {
		this.ganar=false;
	}
	
	public Integer getDimensiones() {
		return this.dimensiones;
	}

	public Grilla getGrilla() {
		return grilla;
	}

	public void setGrilla(int dimensiones) {
		this.grilla = new Grilla(dimensiones);
		this.grilla.iniciarGrilla();
	}

	public Grilla getBuffer() {
		return buffer;
	}

	public void setBuffer(int dimensiones) {
		this.buffer = new Grilla(dimensiones);
	}
	
	public void gano() {
		boolean ganar = true;
		for(int i = 0; i < this.buffer.getColumna().length; i++) ganar = ganar && this.buffer.verificarColumna(i,this.grilla);
		for(int i = 0; i < this.buffer.getFila().length; i++) ganar = ganar && this.buffer.verificarFila(i,this.grilla);
		this.ganar = ganar;
	}
	
	public void iniciarTiempo() {
		//comienza a contar el tiempo luego de dibujar la matriz
		finalizado = false;
		inicio = Instant.now();
	}
	
	public void finalizarTiempo() {
		//Detiene el contador e imprime el tiempo en la consola
		fin = Instant.now();
		if (inicio != null) {
		    duracion = ChronoUnit.SECONDS.between(inicio,fin);
		}
		finalizado = true;
		this.gano();
		System.out.println("el tiempo de juego fue de "+ duracion+" segundos");
		//guarda en el scoreboard el puntaje
		if(isGanar())escribir(duracion);
	}
	
	public boolean estaFinalizado() {
		return this.finalizado;
	}
	
	
	public void verificarNumeros(ArrayList<JTextField> numeros) {
		for (int i=0;i<numeros.size();i++) {
			if (numeros.get(i).getText().isEmpty()) numeros.get(i).setText("0");
		}
	} 
	
	public void completar(ArrayList<CajaDeTexto> numeros) {
		for (int i=0;i<numeros.size();i++) {
			if (numeros.get(i).getCaja().getText().isEmpty()) numeros.get(i).getCaja().setText("0");
		}
	}
	
	public void jugar(int fila, int columna,int numero) {
		this.buffer.jugar(fila, columna, numero);
	}
	
	public ArrayList<Integer> getFilasResueltas() {
		return this.buffer.getFilasBienHechas(grilla);
	}
	
	public ArrayList<Integer> getColumnasResueltas() {
		return this.buffer.getColumnasBienHechas(grilla);
	}
   
	public void setJugador(String jugador) {
		this.jugador = jugador;
	}
	
	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}
	
	public static ArrayList<String[]> leerArchivo() {
		ArrayList<String[]> puntajes=new ArrayList<String[]>();
		File file=new File(".");
        try {
            Scanner input = new Scanner(new File(file.getCanonicalPath()+file.separator+"src"+file.separator+"tpprograII"+file.separator+"scoreboard.txt"));
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String nombre=line.substring(8, line.indexOf(" - Dificultad:"));
                String dificultad=line.substring(8+nombre.length()+14, line.indexOf(" - Duracion:"));
                String tiempo=line.substring(line.indexOf(" - Duracion:")+13, line.length());
                String[] puntaje={nombre, dificultad, tiempo};
                puntajes.add(puntaje);
            }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
        }
        return puntajes;
	}
	
	public static ArrayList<String[]> scoreboardAcomodado(){
		ArrayList<String[]> ret = leerArchivo();
		String aux;
		for (int i=0; i<ret.size();i++) {
			for (int j=i+1; j < ret.size(); j++) {
				if(Integer.parseInt(ret.get(i)[2])>Integer.parseInt(ret.get(j)[2])) {
					for (int x=0; x<ret.get(i).length;x++) {
						aux = ret.get(i)[x];
						ret.get(i)[x]=ret.get(j)[x];
						ret.get(j)[x]=aux;
					}
				}
			}
		}
		return ret;
	}

	
	public static void escribir(long linea){	
		BufferedWriter bw = null;
	    FileWriter fw = null;
	    try {
	        String data = "Jugador: "+jugador+" - Dificultad: "+dificultad+" - Duracion: "+duracion+"\n";
	        File miDir = new File (".");
			String miDireccion = ""; 
			try {
		    	 miDireccion = miDir.getCanonicalPath();
		       }
		     catch(Exception e) {
		    	 e.printStackTrace();
		       }
			//crea el archivo
			File file = new File(miDireccion+ File.separator +"src"+File.separator +"tpprograII"+File.separator +"scoreboard.txt");
	        // Si el archivo no existe, se crea!
	        if (!file.exists()) {
	            file.createNewFile();
	        }
	        // flag true, indica adjuntar información al archivo.
	        fw = new FileWriter(file.getAbsoluteFile(), true);
	        bw = new BufferedWriter(fw);
	        bw.write(data);
	        System.out.println("información agregada!");
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	                        //Cierra instancias de FileWriter y BufferedWriter
	            if (bw != null)
	                bw.close();
	            if (fw != null)
	                fw.close();
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
    }  
}
	
	public boolean isGanar() {
		return this.ganar;
	}
	
	public static void main(String args[]) {
	}

}
