package tpprograII;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Panel;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.GridBagConstraints;
import java.awt.Label;
import java.awt.Button;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Color;

public class pantallaJuego {

	protected static final Component RootPaneContainer = null;
	private JFrame frame;
	private Integer dimensiones;
	private boolean dificultad;
	private Juego juego;
	private Runnable ejecucionFilas, ejecucionColumnas;
	private Thread hiloFilas, hiloColumnas;
	private ArrayList<CajaDeTexto> celdas = new ArrayList<CajaDeTexto>();
	private ArrayList<Etiqueta> etiquetasFilas = new ArrayList<Etiqueta>(), etiquetasColumna = new ArrayList<Etiqueta>();
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Create the application.
	 */
	public pantallaJuego(Integer dimensiones, boolean facil) {
		initialize(dimensiones);
		if(facil) this.modoFacil();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	public void modoFacil() {
		this.ejecucionFilas = new Runnable() {
			@Override
			public void run() {
				boolean hecho = false;
				while(true && !hecho) {
					try {
						Thread.sleep(1000);
						ArrayList<Integer> filas = juego.getFilasResueltas();
						for(Etiqueta f: etiquetasFilas) {
							if(filas.contains(f.getUbicacion())) f.getEtiqueta().setText("Bien Hecho!");
							else f.resetText();
						}
						if(filas.size() == juego.getDimensiones()) hecho = true;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		};
		this.hiloFilas = new Thread(this.ejecucionFilas);
		this.hiloFilas.start();
		
		this.ejecucionColumnas = new Runnable() {
			@Override
			public void run() {
				boolean hecho = false;
				while(true && !hecho) {
					try {
						Thread.sleep(1000);
						ArrayList<Integer> columnas = juego.getColumnasResueltas();
						for(Etiqueta f: etiquetasColumna) {
							if(columnas.contains(f.getUbicacion())) f.getEtiqueta().setText("Bien Hecho!");
							else f.resetText();
						}
						if(columnas.size() == juego.getDimensiones()) hecho = true;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		};
		this.hiloColumnas = new Thread(this.ejecucionColumnas);
		this.hiloColumnas.start();
	
	}
	
	private void initialize(Integer dimensiones) {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(frame);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		juego=new Juego(dimensiones);
		juego.getGrilla().mostrarGrilla();
		
		JButton btnVerificar = new JButton("Verificar");
		btnVerificar.setFont(new Font("Dialog", Font.BOLD, 12));
		btnVerificar.setBounds(320, 219, 104, 32);
		frame.getContentPane().add(btnVerificar);
		
		JButton btnScoreboard = new JButton("Ver Puntajes");
		btnScoreboard.setFont(new Font("Dialog", Font.BOLD, 12));
		btnScoreboard.setBounds(273, 219, 151, 32);
		frame.getContentPane().add(btnScoreboard);
		btnScoreboard.setVisible(false);
		
		
		btnVerificar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int num=0;
				juego.completar(celdas);
				juego.finalizarTiempo();
				//si se presiona el boton verificar, termina el juego, y esconde el boton verificar
				if (juego.estaFinalizado()) {
					btnVerificar.setVisible(false);
					btnScoreboard.setVisible(true);
				}
				juego.gano();
				if(juego.isGanar()) JOptionPane.showMessageDialog(RootPaneContainer, "Has ganado!!");
				else JOptionPane.showMessageDialog(RootPaneContainer, "Has perdido...");
			}
		});
		
		btnScoreboard.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pantallaPuntaje pp=new pantallaPuntaje();
				pp.getFrame().setVisible(true);
				frame.setVisible(false);
				
			}
		});
		
		Panel panel = new Panel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 10, 414, 203);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(juego.getBuffer().getFila().length+1, 4, juego.getBuffer().getColumna().length+1, 4));
		
		
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(pantallaJuego.class.getResource("/images/fondo.jpg")));
		lblNewLabel.setBounds(0, 0, 436, 264);
		frame.getContentPane().add(lblNewLabel);
		

		for(int i = 0; i < juego.getBuffer().getFila().length; i++) {
			for(int j = 0; j < juego.getBuffer().getColumna().length; j++) {
				CajaDeTexto caja = new CajaDeTexto(i,j);
				caja.iniciarCaja(juego);
				panel.add(caja.getCaja());
				celdas.add(caja);
				caja.getCaja().setHorizontalAlignment(SwingConstants.CENTER);
			}
			Etiqueta numFilas = new Etiqueta(i,(Integer.toString(juego.getGrilla().getFila()[i])));
			numFilas.getEtiqueta().setHorizontalAlignment(SwingConstants.CENTER);
			panel.add(numFilas.getEtiqueta());
			etiquetasFilas.add(numFilas);
		}
			
		for (int i = 0; i < juego.getBuffer().getColumna().length; i++) {
			Etiqueta numColumnas = new Etiqueta(i,Integer.toString(juego.getGrilla().getColumna()[i]));
			numColumnas.getEtiqueta().setHorizontalAlignment(SwingConstants.CENTER);
			panel.add(numColumnas.getEtiqueta());
			etiquetasColumna.add(numColumnas);
		}
		juego.iniciarTiempo();
				
	}
}
