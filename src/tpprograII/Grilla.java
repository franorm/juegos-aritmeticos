package tpprograII;
import java.math.*;
import java.util.ArrayList;

public class Grilla {
	private int[][] buffer;
	private int[] columna, fila;
	
	public Grilla(int dimension) {
		if(dimension < 4) throw new IllegalArgumentException("El minimo de la grilla debe ser 4 "+dimension);
		this.setGrilla(dimension);
		this.setFila(dimension);
		this.setColumna(dimension);
	}
	
	public void iniciarGrilla() {
		for(int i = 0; i < this.buffer.length; ++i) {
			int sumaFila = 0;
			for(int j = 0; j < this.buffer.length; ++j) {
				int numero = (int) Math.round(Math.random() * 8 + 1);
				this.buffer[i][j] = numero;
				this.columna[j] += numero; 
				sumaFila += numero;
			}this.fila[i] = sumaFila;
		}
	}
	
	
	public int[][] getGrilla() {
		return buffer;
	}

	public void setGrilla(int dimension) {
		this.buffer = new int[dimension][dimension];
	}

	public int[] getColumna() {
		return columna;
	}

	public void setColumna(int dimension) {
		this.columna = new int [dimension];
	}

	public int[] getFila() {
		return fila;
	}

	public void setFila(int dimension) {
		this.fila = new int [dimension];
	}
	
	public int getResultadoFila(int fila) {
		return this.fila[fila];
	}

	public int getResultadoColumna(int columna) {
		return this.columna[columna];
	}
	
	public void mostrarGrilla() {
		for(int i = 0; i < this.buffer.length; ++i) {
			System.out.print("[");
			for(int j = 0;  j < this.buffer.length; j++) {
				System.out.print(this.buffer[i][j]+" ");
			}System.out.println(this.fila[i]+ " ]");
			
		}
		System.out.print("[");
		for(int j = 0;  j < this.columna.length; j++) {
			System.out.print(this.columna[j]+" ");
		}System.out.println(" ]");
		
	}
	
	public void jugar(int fila, int columna, int numero) {
		int anterior = this.buffer[fila][columna];
		this.buffer[fila][columna] = numero;
		this.fila[fila] +=(numero-anterior);
		this.columna[columna] += (numero-anterior);
	}
	
	public boolean verificarFila(int fila,Grilla grilla) {
		return grilla.getResultadoFila(fila) == this.getResultadoFila(fila);
	}
	
	public boolean verificarColumna(int columna, Grilla grilla) {
		return grilla.getResultadoColumna(columna) == this.getResultadoColumna(columna);
	}
	
	public ArrayList<Integer> getColumnasBienHechas(Grilla grilla) {
		ArrayList<Integer> columnas = new ArrayList<Integer>();
		for(int i = 0; i < this.buffer.length; i++) {
			if(this.verificarColumna(i, grilla)) columnas.add(i);
		}
		return columnas;
	}
	
	public ArrayList<Integer> getFilasBienHechas(Grilla grilla) {
		ArrayList<Integer> filas = new ArrayList<Integer>();
		for(int i = 0; i < this.buffer.length; i++) {
			if(this.verificarFila(i,grilla)) filas.add(i);
		}
		return filas;
	}
	
	public static void main(String args[]) {
		Grilla g = new Grilla(4);
		g.iniciarGrilla();
		g.mostrarGrilla();
	}
	
}
