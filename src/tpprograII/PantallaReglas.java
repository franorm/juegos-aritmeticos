package tpprograII;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.Color;

public class PantallaReglas {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	/**
	 * Create the application.
	 */
	public PantallaReglas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(frame);
		frame.setResizable(false);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(55, 36, 323, 169);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 11, 303, 104);
		panel.add(textPane);
		textPane.setEditable(false);
		textPane.setText("En el juego se presenta una grilla (mnima de 4x4), con nmeros enteros asociados a las filas y las columnas. Usted debe adivinar que nmero poner en cada casillero de tal manera que la suma de cada fila sea igual al valor dado, y que la suma de los nmeros de cada columna sea igual al valor dado.");
		
		JButton btnInicio = new JButton("Volver al Inicio");
		btnInicio.setBounds(240, 216, 138, 34);
		frame.getContentPane().add(btnInicio);
		
		btnInicio.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PantallaInicio pi=new PantallaInicio();
				frame.setVisible(false);
				pi.getFrame().setVisible(true);
				
			}
		});
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(0, 0, 434, 261);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel.setIcon(new ImageIcon(PantallaInicio.class.getResource("/images/fondo.jpg")));
		lblNewLabel.setBounds(0, 0, 436, 264);
		frame.getContentPane().add(lblNewLabel);
		
	}
}
