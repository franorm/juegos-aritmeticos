package tpprograII;
import java.awt.*;
import javax.swing.*;

public class Etiqueta {
	private Integer ubicacion;
	private JLabel etiqueta;
	private String texto;
	
	public Etiqueta(int i, String texto) {
		this.ubicacion = i;
		etiqueta = new JLabel();
		this.texto = texto;
		this.etiqueta.setText(texto);
	}

	public int getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(int ubicacion) {
		this.ubicacion = ubicacion;
	}

	public JLabel getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(JLabel etiqueta) {
		this.etiqueta = etiqueta;
	}
	
	public void resetText() {
		this.etiqueta.setText(this.texto);
	}

}
