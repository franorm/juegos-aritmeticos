package tpprograII;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class pantallaPuntaje {

	private JFrame frame;
	private JLabel lblNewLabel=new JLabel();
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					pantallaPuntaje window = new pantallaPuntaje();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public void setFrame(JFrame frame) {
		this.frame=frame;
	}

	/**
	 * Create the application.
	 */
	public pantallaPuntaje() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(frame);
		frame.setResizable(false);	
		frame.getContentPane().setLayout(null);
		
		
		table = new JTable();
		table.setBounds(38, 31, 367, 179);
		frame.getContentPane().add(table);
		
		
		DefaultTableModel model= new DefaultTableModel();
		model.addColumn("Nombre");
		model.addColumn("Dificultad");
		model.addColumn("Tiempo (segundos)");

		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setBounds(38, 31, 367, 179);
		frame.getContentPane().add(scroll);
		
		JButton btnInicio = new JButton("Volver al Inicio");
		btnInicio.setBounds(277, 216, 128, 23);
		frame.getContentPane().add(btnInicio);
		
		lblNewLabel.setIcon(new ImageIcon(PantallaInicio.class.getResource("/images/fondo.jpg")));
		lblNewLabel.setBounds(0, 0, 436, 264);
		frame.getContentPane().add(lblNewLabel);
		
		ArrayList<String[]> punt=Juego.scoreboardAcomodado();
		
		for (int i=0;i<punt.size();i++) {
			model.addRow(punt.get(i));
		}
		
		table.setModel(model);
		
		
		btnInicio.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PantallaInicio pi=new PantallaInicio();
				pi.getFrame().setVisible(true);
				frame.setVisible(false);			
				
			}
		});
		
		
		
	}
}
