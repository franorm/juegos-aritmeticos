package tpprograII;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import javax.swing.*;

public class CajaDeTexto {
	private int fila, columna;
	private JTextField caja;
	
	CajaDeTexto(int fila, int columna){
		this.setColumna(columna);
		this.setFila(fila);
		this.caja = new JTextField();
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getColumna() {
		return columna;
	}

	public void setColumna(int columna) {
		this.columna = columna;
	}
	
	public void iniciarCaja(Juego juego) {
		this.caja.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				char validar=e.getKeyChar();
				
				if(!Character.isDigit(validar)) {
					e.consume();
				}else{		
					int numero = Integer.parseInt(""+validar);
					juego.jugar(fila, columna, numero);
				}
			}

		});
	}
	
	public JTextField getCaja() {
		return this.caja;
	}
}
